

public class Triangulos
{
    private double ladoABase, ladoB, ladoC;
    
    public Triangulos() {
        this.ladoABase = this.ladoB = this.ladoC = 0;   
    }
    
    public Triangulos(double ladoABase, double ladoB, double ladoC) {
        this.ladoABase = ladoABase;
        this.ladoB = ladoB;
        this.ladoC = ladoC;
    }
    
    public double calculaPerimetro() {
        return this.ladoABase + this.ladoB + this.ladoC;   
    }
    
    //nao sei se isso eh uma boa pratica ou se esta certo
    private double calculaSemiPerimetro() {
        return (this.ladoABase + this.ladoB + this.ladoC) / 2;   
    }
    
    //existem valores que nao funcionam, acredito ser por condicao de existencia.
    //nao tratei isso
    public double calculaArea() {
        if(this.ladoABase == this.ladoB && this.ladoB == this.ladoC) {
            return (Math.sqrt(3) / 4) * Math.pow(ladoB, 2);   
        } else if(this.ladoB == this.ladoC && this.ladoC != this.ladoABase && this.ladoB != this.ladoABase) {
            return (this.ladoABase * (Math.sqrt(Math.pow(ladoB, 2) - (Math.pow((ladoABase / 2), 2)))) / 2);
        } else {
            //teorema de heron
            return Math.sqrt(calculaSemiPerimetro() * (calculaSemiPerimetro() - this.ladoABase) * (calculaSemiPerimetro() - this.ladoB)
            * (calculaSemiPerimetro() - this.ladoC));
        }
    }
    
    public String getTipoTriangulo() {
        if(this.ladoABase == this.ladoB && this.ladoB == this.ladoC) {
            return "equilátero";   
        } else if(this.ladoB == this.ladoC && this.ladoC != this.ladoABase && this.ladoB != this.ladoABase) {
            return "isósceles";
        } else {
            return "escaleno";
        }
    }
    
    public void setLadoABase(double ladoABase) {
        this.ladoABase = ladoABase;   
    }
    
    public double getLadoABase() {
        return this.ladoABase;   
    }
    
    public void setLadoB(double ladoB) {
        this.ladoB = ladoB;
    }
    
    public double getLadoB() {
        return this.ladoB;   
    }
    
    public void setLadoC(double ladoC) {
        this.ladoC = ladoC;
    }
    
    public double getLadoC() {
        return this.ladoC;   
    }
}
