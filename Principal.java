import java.util.Scanner;


public class Principal
{
    public static void main(String[] args) {
        
        Scanner le = new Scanner(System.in);
        
        System.out.print("Informe o tamanho da base(lado a) do triângulo: ");
        Triangulos triangulo1 = new Triangulos();
        triangulo1.setLadoABase(le.nextDouble());
        
        System.out.print("Informe o tamanho do lado b do triângulo: ");
        triangulo1.setLadoB(le.nextDouble());
        
        System.out.print("Informe o tamanho do lado c do triângulo: ");
        triangulo1.setLadoC(le.nextDouble());
        
        //Coloquei em cm², mas nao especifiquei em lugar nenhum
        System.out.print("O perímetro do triângulo informado é de " + triangulo1.calculaPerimetro() + " cm e sua área é de " + triangulo1.calculaArea() 
        + " cm² e o seu tipo é " + triangulo1.getTipoTriangulo() + ".");
    }
}
